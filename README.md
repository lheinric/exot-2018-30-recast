# RECAST WORKFLOW FOR W' -> l+nu ANALYSIS

This runs the W'->lnu analysis as a workflow:

1. Runs the event selection for the electron and muon channels on the supplied mc16a+d+e samples for the signal process of interest
2. Post-processes the outputs into a signal template file with a layout compatible with the statistical analysis
3. Runs the statistical analysis using the newly produced signal template files

As things are currently set up, the statistical analysis that is performed amounts to the calculation of the
observed cross section limit for the electron and muon channels individually as well as the combined limit.

The setup is in general heavily inspired by and based on the one found here:
https://gitlab.cern.ch/recast-atlas/susy/ATLAS-CONF-2018-041/

## Required Input

The inputs are to be provided as in test/input.yml. The derivation files must be in DAOD_EXOT9 format
for the electron channel and DAOD_EXOT17 format for the muon channel and should be provided as an XrootD directory path
containing individual subdirectories corresponding to the mc16a/d/e samples. The corresponding NTUP_PILEUP files
need to be made available locally where the code should be run.

The inputs specified in test/input.yml are as follows:

XrootD paths to the electron and muon channel DAOD input files:
```
inputdata_xrootd_el: 'root://eosuser.cern.ch//eos/user/m/mkbugge/recastTestInputs/electron'
inputdata_xrootd_mu: 'root://eosuser.cern.ch//eos/user/m/mkbugge/recastTestInputs/muon'
```
It is assumed that the subdirectories of these directories have the names of the relevant datasets, i.e. as
obtained by running a "rucio get" command from within the top-level electron or muon channel directories.

Maximum signal cross section in pb:
```
maxSigmaSig: '0.001'
```
One should ensure that this has a sensible value by comparing to the obtained limits. If the limit is close to
maxSigmaSig, it indicates that the limit calculation has failed due to "hitting the wall" in terms of cross section, and
if it is too small compared to maxSigmaSig, the binning of the cross section axis may not be suitable to obtain
a precise result. As a rule of thumb, if the limit is between 10% and 50% of maxSigmaSig you are definitely safe.

Number of events to process within each MC campaign, where "-1" means to process all events and should in general
be used except for testing purposes:
```
NevtsToProcess: '-1'
```

NTUP_PILEUP file paths:
```
PU_config_el_mc16a: 'pileUpConfigs/electronChannel/PURW_mc16a.root'
PU_config_el_mc16d: 'pileUpConfigs/electronChannel/PURW_mc16d.root'
PU_config_el_mc16e: 'pileUpConfigs/electronChannel/PURW_mc16e.root'
PU_config_mu_mc16a: 'pileUpConfigs/muonChannel/PURW_mc16a.root'
PU_config_mu_mc16d: 'pileUpConfigs/muonChannel/PURW_mc16d.root'
PU_config_mu_mc16e: 'pileUpConfigs/muonChannel/PURW_mc16e.root'
```


## Running the Reinterpretation

The following procedure should work to run the workflow.

1. Clone the repository locally and go into the top-level directory "exot-2018-30-recast"

2. Ensure that the necessary credentials can be obtained to access the XrootD input directories:
```
mkdir auth
echo "echo <password>|kinit <username>@CERN.CH" > auth/getkrb.sh
chmod a+x auth/getkrb.sh
export PACKTIVITY_AUTH_LOCATION=$PWD/auth
```

3. Run the workflow
```
yadage-run workdir specs/workflow.yml test/input.yml -d initdir=$PWD/test 
```


## Results

The outputs of the statistical analysis can be seen as follows:
```
$ cat workdir/statAnalysis/_packtivity/statAnalysis.run.log | grep 'cross section limit'
2019-05-21 14:10:34,794 | pack.statAnalysis.ru |   INFO | cross section limit [pb]: 6.30178e-05
2019-05-21 14:26:29,141 | pack.statAnalysis.ru |   INFO | cross section limit [pb]: 0.000360356
2019-05-21 15:09:06,896 | pack.statAnalysis.ru |   INFO | cross section limit [pb]: 6.09559e-05
```
These are the observed cross section limits obtained for the electron and muon channels and the combination.
As detailed above, care must be taken to ensure a suitable value for the maxSigmaSig parameter has been
used.


## Software

The analysis runs based on three docker images:

1. Electron channel event selection: 
   - Repo: https://gitlab.cern.ch/atlas-phys/exot/lpx/EXOT-2018-30-Projects/exot-2018-30-electronanalysis
   - Image: gitlab-registry.cern.ch/atlas-phys/exot/lpx/exot-2018-30-projects/exot-2018-30-electronanalysis

2. Muon channel event selection:
   - Repo: https://gitlab.cern.ch/atlas-phys/exot/lpx/EXOT-2018-30-Projects/exot-2018-30-muonanalysis
   - Image: gitlab-registry.cern.ch/atlas-phys/exot/lpx/exot-2018-30-projects/exot-2018-30-muonanalysis

3. Statistical analysis: 
   - Repo: https://gitlab.cern.ch/atlas-phys/exot/lpx/EXOT-2018-30-Projects/exot-2018-30-statisticalanalysis
   - Image: gitlab-registry.cern.ch/atlas-phys/exot/lpx/exot-2018-30-projects/exot-2018-30-statisticalanalysis

